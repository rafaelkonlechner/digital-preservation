#!/bin/bash
#
# Experiment 2:
# Migration from ARW to DNG (compressed).
#
# Compressed DNG's must be created manually using Adobe DNG Converter
# Path to compressed DNG's must be provided as an argument

dngPath="manually/dng_compressed"

mkdir -p manually
mkdir -p $dngPath

sh arw2dng-exp-template.sh "2" "$dngPath"
