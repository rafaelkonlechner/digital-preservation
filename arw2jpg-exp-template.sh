#!/bin/bash
#
# Experiment:
# Migration from ARW to JPEG.
#
# a) render manually with Sony Image Data Converter (reference)
# b) If the flag -m is set: 
#    manually rendered JPEGs must be provided under /manually/jpg
#    (e.g. renderd manually with macOS Preview)
#    Otherwise the JPEGs will be rendered fully automatized using sips

process_check()
{
	exit_code=$1
	ret_val=$2

	echo "$ret_val"
	if [ $exit_code == 0 ]; then
		grep -qi "True" <<< $ret_val
		success=$?
		if [ $success == 0 ]; then
			# check was true
			return 1
		else
			# check was false
			return 0
		fi
	else
		# error occured in check
		return -1
	fi
}

process_calc()
{
	exit_code=$1
	echo "$2"

	if [ $exit_code == 0 ]; then
		# calc was successful
		return 0;
	else
		# error occured in calc
		return -1
	fi
}

## Main script ##

experimentNr=$1
manualFlag=0
if [ $# -gt 1 ] && [ "$2" == "-m" ]; then
	manualFlag=1
fi

echo "Running experiment ${experimentNr}"

sony_path="manually/jpg_sony"

mkdir -p manually
mkdir -p "${sony_path}"
mkdir -p tmp
mkdir -p out

if [ $manualFlag == 1 ]; then
	other_path="manually/jpg"
else
	other_path="tmp/jpg_sips"
fi

mkdir -p "${other_path}"

exp_name="exp${experimentNr}"
timestamp=$(date +%s)
echo "Sample,Width,Height,Color space,Camera model,Storage consumption %,MSE,SSIM" >> "out/${exp_name}_${timestamp}.csv"

for arw in samples/*.ARW
do
	filename=$(basename "$arw")
	results="$filename,"
	
	sony_jpg="${sony_path}/${filename%.*}.JPG"
	if ! [ -f "$sony_jpg" ]; then
		echo "File ${sony_jpg} does not exists. Please create it with Sony Image Data Converter"
		exit 1
	fi
	
	other_jpg="${other_path}/${filename%.*}.jpg"
	if [ $manualFlag == 1 ]; then
		if ! [ -f "$other_jpg" ]; then
			echo "File ${other_jpg} does not exists. Please create it manually, e.g. with macOS Preview"
			exit 1
		fi
	else
		sh convert/convert-arw-2-jpg.sh "$arw" "$other_jpg"
	fi

	# -- Compare JPEG's --
	# Checks
	ret=$(sh check/check-width-equal.sh "$sony_jpg" "$other_jpg")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-height-equal.sh "$sony_jpg" "$other_jpg")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-colorspace-equal.sh "$sony_jpg" "$other_jpg")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-model-equal.sh "$sony_jpg" "$other_jpg")
	process_check $? "$ret"
	results="${results}$?,"
	
	# Calcs
	ret=$(sh calc/calc-relative-storage-consumption.sh "$arw" "$other_jpg")
	process_calc $? "$ret"
	results="${results}${ret},"

	ret=$(sh calc/calc-mse.sh "$sony_jpg" "$other_jpg" "tmp/diff.jpg")
	process_calc $? "$ret"
	results="${results}${ret},"

	ret=$(sh calc/calc-ssim.sh "$sony_jpg" "$other_jpg")
	process_calc $? "$ret"
	results="${results}${ret}"

	echo ${results} >> "out/${exp_name}_${timestamp}.csv"

done

exit 0

