#!/bin/bash

echo "Convert $1 to $2"
dcraw -c -T -6 "$1" > "$2"
