#!/bin/bash
#
# Experiment 3:
# Migration from ARW to TIFF (8bit).
#
# a) render manually with Sony Image Data Converter (reference)
# b) render with dcraw

process_check()
{
	exit_code=$1
	ret_val=$2

	echo "$ret_val"
	if [ $exit_code == 0 ]; then
		grep -qi "True" <<< $ret_val
		success=$?
		if [ $success == 0 ]; then
			# check was true
			return 1
		else
			# check was false
			return 0
		fi
	else
		# error occured in check
		return -1
	fi
}

process_calc()
{
	exit_code=$1
	echo "$2"

	if [ $exit_code == 0 ]; then
		# calc was successful
		return 0;
	else
		# error occured in calc
		return -1
	fi
}

## Main script ##

echo "Running experiment 3"

sony_path="manually/tiff_sony"

mkdir -p manually
mkdir -p "${sony_path}"
mkdir -p tmp
mkdir -p tmp/arw2tiff
mkdir -p out

exp_name="exp3"
timestamp=$(date +%s)
echo "Sample,Width,Height,Color space,Camera model,Storage consumption %,MSE,SSIM" >> "out/${exp_name}_${timestamp}.csv"

for arw in samples/*.ARW
do
	filename=$(basename "$arw")
	results="$filename,"

	dcraw_tiff="tmp/arw2tiff/${filename%.*}.tiff"
	sh convert/convert-arw-dng-2-tiff.sh "$arw" "$dcraw_tiff"

	sony_tiff="${sony_path}/${filename%.*}.TIF"
	if ! [ -f "$sony_tiff" ]; then
		echo "File ${sony_tiff} does not exists. Please create it with Sony Image Data Converter"
		exit 1
	fi

	# -- Compare TIFF's --
	# Checks
	ret=$(sh check/check-width-equal.sh "$sony_tiff" "$dcraw_tiff")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-height-equal.sh "$sony_tiff" "$dcraw_tiff")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-colorspace-equal.sh "$sony_tiff" "$dcraw_tiff")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-model-equal.sh "$sony_tiff" "$dcraw_tiff")
	process_check $? "$ret"
	results="${results}$?,"

	# Calcs
	ret=$(sh calc/calc-relative-storage-consumption.sh "$arw" "$dcraw_tiff")
	process_calc $? "$ret"
	results="${results}${ret},"

	ret=$(sh calc/calc-mse.sh "$sony_tiff" "$dcraw_tiff" "tmp/diff.tiff" | sed -e "s/identify\(.\)*//g" | sed -e "s/compare\(.\)*//g")
	process_calc $? "$ret"
	results="${results}${ret},"

	ret=$(sh calc/calc-ssim.sh "$sony_tiff" "$dcraw_tiff" | sed -e 's/---.*---//g')
	process_calc $? "$ret"
	results="${results}${ret}"

	echo ${results} >> "out/${exp_name}_${timestamp}.csv"

done

exit 0
