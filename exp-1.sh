#!/bin/bash
#
# Experiment 1:
# Migration from ARW to DNG (uncompressed).
#
# Uncompressed DNG's must be created manually using Adobe DNG Converter
# Path to uncompressed DNG's must be provided as an argument

dngPath="manually/dng"

mkdir -p manually
mkdir -p $dngPath

sh arw2dng-exp-template.sh "1" "$dngPath"
