#!/bin/bash
#
# Experiment 5:
# Migration from ARW to TIFF (8bit) with Sony.
#

process_check()
{
	exit_code=$1
	ret_val=$2

	echo "$ret_val"
	if [ $exit_code == 0 ]; then
		grep -qi "True" <<< $ret_val
		success=$?
		if [ $success == 0 ]; then
			# check was true
			return 1
		else
			# check was false
			return 0
		fi
	else
		# error occured in check
		return -1
	fi
}

process_calc()
{
	exit_code=$1
	echo "$2"

	if [ $exit_code == 0 ]; then
		# calc was successful
		return 0;
	else
		# error occured in calc
		return -1
	fi
}

## Main script ##

echo "Running experiment 5"

sony_path="manually/tiff_sony"

mkdir -p manually
mkdir -p "${sony_path}"
mkdir -p tmp
mkdir -p tmp/arw2tiff
mkdir -p out

exp_name="exp5"
timestamp=$(date +%s)
echo "Sample,Width,Height,Color space,Camera model,Storage consumption %,MSE,SSIM" >> "out/${exp_name}_${timestamp}.csv"

for arw in samples/*.ARW
do
	filename=$(basename "$arw")
	results="$filename,"

	sony_tiff="${sony_path}/${filename%.*}.TIF"
	if ! [ -f "$sony_tiff" ]; then
		echo "File ${sony_tiff} does not exists. Please create it with Sony Image Data Converter"
		exit 1
	fi

	# -- Compare TIFF's --
	# Checks
	ret=$(sh check/check-width-equal.sh "$sony_tiff" "$arw")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-height-equal.sh "$sony_tiff" "$arw")
	process_check $? "$ret"
	results="${results}$?,"

	#ret=$(sh check/check-colorspace-equal.sh "$sony_tiff" "$arw")
	#process_check $? "$ret"
	results="${results}N,"

	#ret=$(sh check/check-model-equal.sh "$sony_tiff" "$arw")
	#process_check $? "$ret"
	results="${results}N,"

	# Calcs
	ret=$(sh calc/calc-relative-storage-consumption.sh "$arw" "$sony_tiff")
	process_calc $? "$ret"
	results="${results}${ret},"

	results="${results}0.0,1.0"
	echo ${results} >> "out/${exp_name}_${timestamp}.csv"

done

exit 0
