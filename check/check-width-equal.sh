#!/bin/bash

X=$(identify -format "%[fx:w] " "$1")
Y=$(identify -format "%[fx:w] " "$2")
A=$(dcraw -i -v $2 | grep "Image size" | sed -e "s/Image size:..//g" | sed -e "s/x.[0-9]*.$//g")

if [ -z "${X}" -o "${X}" == " " ] || [ -z "${Y}" -o "${Y}" == " " ]; then

    if [ -z "${Y}" -o "${Y}" == " " ]; then
	       echo "Comparing RAW"
            if [ "${X}" == "${A}" ]; then
        	       echo "True"
            else
        	       echo "False"
            fi
            exit 0
    else
	       echo "error: comparing empty strings"
	       exit 1
    fi
fi

echo "Compare width $X vs $Y"

if [ "${X}" == "${Y}" ]; then
	echo "True"
else
	echo "False"
fi

exit 0
