#!/bin/bash

X=$(wc -c < "$1")
Y=$(wc -c < "$2")

if [ -z "${X}" -o "${X}" == " " ] || [ -z "${Y}" -o "${Y}" == " " ]; then
	echo "error: comparing empty strings"
	exit 1
fi

echo "Compare file size $Y greater than $X"

if [ "${Y}" -gt "${X}" ]; then
	echo "True"
else 
	echo "False"
fi

exit 0
