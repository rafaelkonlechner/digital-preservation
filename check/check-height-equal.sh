#!/bin/bash

X=$(identify -format "%[fx:h] " "$1")
Y=$(identify -format "%[fx:h] " "$2")
A=$(dcraw -i -v $2 | grep "Image size" | sed -e "s/Image size:..[0-9]*.x.//g")

if [ -z "${X}" -o "${X}" == " " ] || [ -z "${Y}" -o "${Y}" == " " ]; then
    if [ -z "${Y}" -o "${Y}" == " " ]; then
	       echo "Comparing RAW"
            if [ "${X}" == "expr ${A} - 25" ]; then
        	       echo "True"
            else
        	       echo "False"
            fi
            exit 0
    else
	       echo "error: comparing empty strings"
	       exit 1
    fi
fi

echo "Compare height $X vs $Y"

if [ "${X}" == "${Y}" ]; then
	echo "True"
else
	echo "False"
fi

exit 0
