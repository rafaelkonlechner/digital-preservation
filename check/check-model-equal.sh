#!/bin/bash

check_not_empty()
{
	if [ -z "$1" -o "$1" == " " ] || [ -z "$2" -o "$2" == " " ]; then
		return 0
	fi
	return 1
}

## Main script ##

X=$(identify -format "%[TIFF:model] " "$1")
Y=$(identify -format "%[TIFF:model] " "$2")
A=$(dcraw -i -v $2 | grep "Camera:" | sed -e "s/Camera:.Sony.//g")

check_not_empty $X $Y
notempty=$?
if [ $notempty == 0 ]; then
	# try EXIF tag
	X=$(identify -format "%[EXIF:model] " "$1")
    Y=$(identify -format "%[EXIF:model] " "$2")
fi

check_not_empty $X $Y
notempty=$?

if [ $notempty == 0 ]; then
    Y=$A
fi

check_not_empty $X $Y
notempty=$?
if [ $notempty == 0 ]; then
	echo "error: comparing empty strings"
	exit 1
fi

echo "Compare camera model $X vs $Y"
echo $X
echo $Y


if [ "${X}" == "${Y}" ]; then
	echo "True"
else
	echo "False"
fi

exit 0
