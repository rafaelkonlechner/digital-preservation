#!/bin/bash

X=$(identify -format "%[colorspace] " "$1")
Y=$(identify -format "%[colorspace] " "$2")

if [ -z "${X}" -o "${X}" == " " ] || [ -z "${Y}" -o "${Y}" == " " ]; then
	echo "error: comparing empty strings"
	exit 1
fi

echo "Compare colorspace $X vs $Y"

if [ "${X}" == "${Y}" ]; then
	echo "True"
else 
	echo "False"
fi

exit 0
