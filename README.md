## How to run the experiments? ##
The experiments can be run in an automated fashion. For each experiment a shell script exp-{1...6}.sh is provided. To execute all experiments sequentially simply run `sh run-experiments.sh`

### Prerequisites ####
To perform the experiments certain tools are required. Also the sample *.ARW files must be copied to the folder `/samples`

#### Required tools #####
* (Sony) Image Data Converter v5
* Adobe DNG Converter v9.9
* dcraw v9.27
* sips (macOS) v10.4.4
* Preview (macOS) v8.0
* ImageMagick v7.0.5

The experiments involve some manual steps due to the usage of some GUI tools for conversions. These manual steps must be performed before starting the shell script(s). The following list describes for each experiment which manual steps must be made in advance.

* **Experiment 1:** Use Adobe DNG Converter to convert the sample *.ARW files into `/manually/dng`. Use the default .dng format.
* **Experiment 2:** Use Adobe DNG Converter to convert the sample *.ARW files into `/manually/dng_compressed`. Use the .dng format with lossy compression (preserve pixel count).
* **Experiment 3:** Use Image Data Converter to convert the sample *.ARW files into `/manually/tiff_sony`. Use the 8-bit TIFF format.
* **Experiment 4:** Use Image Data Converter to convert the sample *.ARW files into `/manually/jpg_sony`. Use the 8-bit JPEG format, compression level 1 (high quality).
* **Experiment 5:** same as in Experiment 3
* **Experiment 6:** same as in Experiment 4
## Where to get the experiment results? ##
For each experiment a CSV file containing the results for all samples is created into the `/out` folder. When re-executing the experiments new CSV files appended with the current UNIX time are created (e.g. exp1_1494615544.csv).