#!/bin/bash
#
# Experiment:
# Migration from ARW to DNG.
#
# Steps (for all samples/*.ARW):
# - Convert ARW to TIFF into tmp/arw2tiff with dcraw
# - Convert DNG to TIFF into tmp/dng2tiff with dcraw
# - Compare TIFF's with ImageMagick
#

process_check()
{
	exit_code=$1
	ret_val=$2

	echo "$ret_val"
	if [ $exit_code == 0 ]; then
		grep -qi "True" <<< $ret_val
		success=$?
		if [ $success == 0 ]; then
			# check was true
			return 1
		else
			# check was false
			return 0
		fi
	else
		# error occured in check
		return -1
	fi
}

process_calc()
{
	exit_code=$1
	echo "$2"

	if [ $exit_code == 0 ]; then
		# calc was successful
		return 0;
	else
		# error occured in calc
		return -1
	fi
}

## Main script ##

experimentNr=$1
dngPath=$2

echo "Running experiment ${experimentNr}"

mkdir -p tmp
mkdir -p tmp/arw2tiff
mkdir -p tmp/dng2tiff
mkdir -p out

exp_name="exp${experimentNr}"
timestamp=$(date +%s)
echo "Sample,Width,Height,Color space,Camera model,Storage consumption %,MSE,SSIM" >> "out/${exp_name}_${timestamp}.csv"

for arw in samples/*.ARW
do
	filename=$(basename "$arw")
	results="$filename,"
	
	arw_tiff="tmp/arw2tiff/${filename%.*}.tiff"
	sh convert/convert-arw-dng-2-tiff.sh "$arw" "$arw_tiff"
	
	dng="${dngPath}/${filename%.*}.dng"
	if ! [ -f "$dng" ]; then
		echo "File $dng does not exists. Please create it with Adobe DNG Converter"
		exit 1
	fi

	dng_tiff="tmp/dng2tiff/${filename%.*}.tiff"
	sh convert/convert-arw-dng-2-tiff.sh "$dng" "$dng_tiff"

	# -- Compare TIFF's --
	# Checks
	ret=$(sh check/check-width-equal.sh "$arw_tiff" "$dng_tiff")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-height-equal.sh "$arw_tiff" "$dng_tiff")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-colorspace-equal.sh "$arw_tiff" "$dng_tiff")
	process_check $? "$ret"
	results="${results}$?,"

	ret=$(sh check/check-model-equal.sh "$arw_tiff" "$dng_tiff")
	process_check $? "$ret"
	results="${results}$?,"

	# Calcs
	ret=$(sh calc/calc-relative-storage-consumption.sh "$arw" "$dng")
	process_calc $? "$ret"
	results="${results}${ret},"

	ret=$(sh calc/calc-mse.sh "$arw_tiff" "$dng_tiff" "tmp/diff.tiff")
	process_calc $? "$ret"
	results="${results}${ret},"

	ret=$(sh calc/calc-ssim.sh "$arw_tiff" "$dng_tiff")
	process_calc $? "$ret"
	results="${results}${ret}"

	echo ${results} >> "out/${exp_name}_${timestamp}.csv"

done

exit 0
