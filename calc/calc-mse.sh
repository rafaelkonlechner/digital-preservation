#!/bin/bash
#
# Calculate Mean Square Error (MSE)
# before calculating the MSE, crop the larger
# image to the dimension of the smaller one

height1=$(identify -format "%[fx:h] " "$1")
width1=$(identify -format "%[fx:w] " "$1")

height2=$(identify -format "%[fx:h] " "$2")
width2=$(identify -format "%[fx:w] " "$2")

if [ "${height1}" -lt "${height2}" -o "${width1}" -lt "${width2}" ]; then
	convert "$2" -crop "${width1}x${height1}+0+0" "tmp/cropped.tiff"
    mse=$(compare -metric MSE "$1" "tmp/cropped.tiff" "$3" 2>&1)
elif [ "${height2}" -lt "${height1}" -o "${width2}" -lt "${width1}" ]; then
	convert "$1" -crop "${width2}x${height2}+0+0" "tmp/cropped.tiff"
	mse=$(compare -metric MSE "tmp/cropped.tiff" "$2" "$3" 2>&1)
else
	mse=$(compare -metric MSE "$1" "$2" "$3" 2>&1)
fi

echo "${mse}"
