#!/bin/bash

X=$(wc -c < "$1")
Y=$(wc -c < "$2")

consumption=$(bc -l <<< "scale=2; 100*$Y/$X")
echo $consumption
